Ext.define('Oscars2015.store.Categories', {
    extend: 'Ext.data.Store',

    config: {
    	model: 'Oscars2015.model.Categoria',

    	proxy: {
    		type: 'ajax',
    		url: 'http://oscars2015.vilma.cat/api/Categories',
        	reader: {
        		type: 'json'
        	}
    	}
    }
});
Ext.define('Oscars2015.store.Participants', {
    extend: 'Ext.data.Store',

    config: {
    	model: 'Oscars2015.model.Participant',

    	proxy: {
    		type: 'ajax',
    		url: 'http://oscars2015.vilma.cat/api/Participants',
        	reader: {
        		type: 'json'
        	}
    	}
    }
});
Ext.define('Oscars2015.store.Nominats', {
    extend: 'Ext.data.Store',

    config: {
    	model: 'Oscars2015.model.Nominat',

    	proxy: {
    		type: 'ajax',
    		url: 'http://oscars2015.vilma.cat/api/Nominats',
        	reader: {
        		type: 'json'
        	}
    	}
    }
});
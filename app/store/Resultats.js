Ext.define('Oscars2015.store.Resultats', {
    extend: 'Ext.data.Store',

    config: {
    	model: 'Oscars2015.model.Resultat',

    	proxy: {
    		type: 'ajax',
    		url: 'http://oscars2015.vilma.cat/api/Resultats',
        	reader: {
        		type: 'json'
        	}
    	}
    }
});
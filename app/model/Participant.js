Ext.define('Oscars2015.model.Participant', {
    extend: 'Ext.data.Model',

    config: {
    	idProperty: 'UsuariId',

    	fields: [ 
    		{ name: 'UsuariId', type: 'int' }, 
    		{ name: 'Nom', type: 'string' },
    		{ name: 'Risk', type: 'float' }
    	]
    }
});
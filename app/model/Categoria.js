Ext.define('Oscars2015.model.Categoria', {
    extend: 'Ext.data.Model',

    config: {
    	idProperty: 'CategoriaId',

    	fields: [ 
    		{ name: 'CategoriaId', type: 'int' }, 
    		{ name: 'Nom', type: 'string' },
            { name: 'VotId', type: 'int' },
    		{ name: 'Vot', type: 'string' },
    		{ name: 'Vots', type: 'int' },
            { name: 'Preu', type: 'int' },
            { name: 'Guanyador', type: 'int' },
    		{ name: 'VotsCategoria', type: 'int' },
            {
                name: 'Percent',
                convert: function(v, record) 
                {
                    var vots = record.get('Vots');

                    if (vots==0) return '0%';

                    var total = record.get('VotsCategoria');

                    var percent = (vots * 100) / total;

                    return  Math.round(percent * 100) / 100 + '%';
                }
            },
            {
                name: 'Guany',
                convert: function(v, record)
                {
                    var vots = record.get('Vots');

                    if (vots==0) return '0€';

                    var total = record.get('VotsCategoria');
                    var preu = record.get('Preu');

                    var apostat = total * preu;
                    var guany = apostat / vots;

                    return  Math.round(guany * 100) / 100 + '€';
                }
            }
    	]
    }
});
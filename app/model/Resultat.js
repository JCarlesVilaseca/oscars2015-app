Ext.define('Oscars2015.model.Resultat', {
    extend: 'Ext.data.Model',

    config: {
    	idProperty: 'UsuariId',

    	fields: [ 
    		{ name: 'UsuariId', type: 'int' }, 
    		{ name: 'Nom', type: 'string' },
    		{ name: 'Guany', type: 'float' }
    	]
    }
});
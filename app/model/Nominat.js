Ext.define('Oscars2015.model.Nominat', {
    extend: 'Ext.data.Model',

    config: {
    	idProperty: 'NominatId',

    	fields: [ 
    		{ name: 'NominatId', type: 'int' }, 
    		{ name: 'Titol', type: 'string' },
    		{ name: 'Subtitol', type: 'string' },
    		{ name: 'Votat', type: 'bool' },
            { name: 'Guanyador', type: 'bool' },
            { name: 'Preu', type: 'int' },
    		{ name: 'Vots', type: 'int' },
    		{ name: 'VotsCategoria', type: 'int' },

    		{
    			name: 'Percent',
    			convert: function(v, record) 
                {
                	var vots = record.get('Vots');

                	if (vots==0) return '0%';

                	var total = record.get('VotsCategoria');

                    var percent = (vots * 100) / total;

                    return  Math.round(percent * 100) / 100 + '%';
                }
    		},
            {
                name: 'Guany',
                convert: function(v, record)
                {
                    var vots = record.get('Vots');

                    var votat = record.get('Votat');

                    if (!votat) vots++;

                    var total = record.get('VotsCategoria');
                    var preu = record.get('Preu');

                    var apostat = total * preu;
                    var guany = apostat / vots;

                    return  Math.round(guany * 100) / 100 + '€';
                }
            }
    	]
    }
});
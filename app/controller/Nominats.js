Ext.define('Oscars2015.controller.Nominats', {
    extend: 'Ext.app.Controller',

    config: {
        control: {
            nominats: {
                itemtap: 'onItemTap'
            }
        }
    },

    onItemTap: function(list,index,item,record) {

        var avui = new Date();

        if (avui.getFullYear()>2015 || avui.getMonth()>1 || avui.getDate()>22)
        {
            Ext.Msg.alert('Votar',"Ha finalitzat el plaç per votar");
            return;
        }

        Ext.Viewport.setMasked({
            xtype: 'loadmask',
            message: 'Enviant...'
        })

        Ext.Ajax.request({
        	url: 'http://oscars2015.vilma.cat/api/Vota',
        	method: 'POST',
        	params: {
        		Usuari: localStorage.getItem('Usuari'),
        		Nominat: record.get('NominatId')
        	},
        	success: function(response) {
        		var result = Ext.JSON.decode(response.responseText);

        		if (result)
        		{
        			var categories = Ext.StoreManager.lookup('Categories');

        			categories.load(
        				{
        					params: 
        					{ 
        						Usuari: localStorage.getItem('Usuari') 
        					},
        					callback: function() {
				                Ext.Viewport.setMasked(null);
				                list.up('navigationview').pop();
        					}
        				}
        			);
        		}
        		else
        		{
	                Ext.Viewport.setMasked(null);
	                Ext.Msg.alert('Votar', "No s'ha pogut enviar el vot");
        		}
        	},
        	failure: function(response) {
                Ext.Viewport.setMasked(null);
                Ext.Msg.alert('Votar', "No s'ha pogut enviar el vot");
        	}
        });
    }
});

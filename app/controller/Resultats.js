Ext.define('Oscars2015.controller.Resultats', {
    extend: 'Ext.app.Controller',

    config: {
        control: {
            resultats: {
                initialize: 'onInit',
                itemtap: 'onItemTap'
            }
        }
    },

    onInit: function(list) {
        Ext.Viewport.setMasked({
            xtype: 'loadmask',
            message: 'Carregant...'
        })

        list.getStore().load(
            {
                params: 
                    { 
                        Usuari: localStorage.getItem('Usuari')
                    },
                callback: function() {
                    Ext.Viewport.setMasked(null);
                }
            });
    },

    onItemTap: function(list,index,item,record) {
        var resultats = Ext.create('Oscars2015.view.ResultatsUsuari',
            { 
                title: record.get('Nom')
            });
        
        Ext.Viewport.setMasked({
            xtype: 'loadmask',
            message: 'Carregant...'
        })

        resultats.getStore().load(
            {
                params: 
                    { 
                        Usuari: record.get('UsuariId') 
                    },

                callback: function() {
                    Ext.Viewport.setMasked(null);
                    list.up('navigationview').push(resultats);
                }
            });
    }
});

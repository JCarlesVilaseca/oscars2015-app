Ext.define('Oscars2015.controller.Participants', {
    extend: 'Ext.app.Controller',

    config: {
        control: {
            participants: {
                initialize: 'onInit'
            }
        }
    },

    onInit: function(list) {
        Ext.Viewport.setMasked({
            xtype: 'loadmask',
            message: 'Carregant...'
        })

        list.getStore().load(
            {
                params: 
                    { 
                        Usuari: localStorage.getItem('Usuari')
                    },
                callback: function() {
                    Ext.Viewport.setMasked(null);
                }
            });
    }
});

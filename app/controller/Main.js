Ext.define('Oscars2015.controller.Main', {
    extend: 'Ext.app.Controller',

    config: {
    	refs: {
    		main: 'main'
    	},

        control: {
            main: {
                initialize: 'onInit'
            }
        }
    },

	getQueryVariable: function(variable) {
	    var query = window.location.search.substring(1);
	    var vars = query.split('&');
	    for (var i = 0; i < vars.length; i++) {
	        var pair = vars[i].split('=');
	        if (decodeURIComponent(pair[0]) == variable) {
	            return decodeURIComponent(pair[1]);
	        }
	    }
	},

    onInit: function(list) {

    	var me = this;

    	var usuari = this.getQueryVariable('Usuari');

    	if (!usuari)
	    	usuari = localStorage.getItem('Usuari');
	    else
	    	localStorage.setItem('Usuari',usuari);

    	if (!usuari)
    	{
    		Ext.Msg.alert('Inici', "El primer cop cal executar la aplicació amb la URL que t'han proporcionat", function() {
    			location.reload();
    		});
    	}
    	else
    	{
	    	me.getMain().push(new Ext.create('Oscars2015.view.Menu'));

	    	var avui = new Date();

	    	if (avui.getFullYear()>2015 || avui.getMonth()>1 || avui.getDate()>22)
	    		me.getMain().push(new Ext.create('Oscars2015.view.Resultats'));
	    	else
	    		me.getMain().push(new Ext.create('Oscars2015.view.Categories'));
	    }
    }
});

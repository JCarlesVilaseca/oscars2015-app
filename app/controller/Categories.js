Ext.define('Oscars2015.controller.Categories', {
    extend: 'Ext.app.Controller',

    config: {
        control: {
            categories: {
                initialize: 'onInit',
                itemtap: 'onItemTap'
            }
        }
    },

    onInit: function(list)
    {
        Ext.Viewport.setMasked({
            xtype: 'loadmask',
            message: 'Carregant dades...'
        })

        list.getStore().load(
            {
                params: 
                { 
                    Usuari: localStorage.getItem('Usuari') 
                },
                callback: function(records, operation, success) {
                    Ext.Viewport.setMasked(null);
                    if (!success)
                    {
                        Ext.Msg.alert('Inici', "No s'ha pogut carregar les dades", function() {
                            location.reload();
                        });
                    }
                }
            }
        );
    },

    onItemTap: function(list,index,item,record) {
        var nominats = Ext.create('Oscars2015.view.Nominats',
            { 
                title: record.get('Nom')
            });
        
        Ext.Viewport.setMasked({
            xtype: 'loadmask',
            message: 'Carregant...'
        })

        nominats.getStore().load(
            {
                params: 
                    { 
                        Usuari: localStorage.getItem('Usuari'), 
                        Categoria: record.get('CategoriaId') 
                    },

                callback: function() {
                    Ext.Viewport.setMasked(null);
                    list.up('navigationview').push(nominats);
                }
            });
    }
});

Ext.define('Oscars2015.controller.Menu', {
    extend: 'Ext.app.Controller',

    config: {
        control: {
            menu: {
                initialize: 'onInit',
                itemtap: 'onItemTap'
            }
        }
    },

    onInit: function(list) {
        var menu = [];

        var avui = new Date();

        if (avui.getFullYear()>2015 || avui.getMonth()>1 || avui.getDate()>22)
            menu.push(
            {
                Titol: 'Resultats',
                Panel: 'Oscars2015.view.Resultats'
            });

        menu.push({
            Titol: 'Categories',
            Panel: 'Oscars2015.view.Categories'
        });

        menu.push(
        {
            Titol: 'Participants',
            Panel: 'Oscars2015.view.Participants'
        });

        menu.push({
            Titol: 'Instruccions',
            Panel: 'Oscars2015.view.Instruccions'
        });

        list.setData(menu);
    },

    onItemTap: function(list,index,item,record) {
        var panel = Ext.create(record.get('Panel'));
        
        list.up('navigationview').push(panel);
    }
});

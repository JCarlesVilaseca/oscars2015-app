    Ext.define('Oscars2015.view.Menu', {
    extend: 'Ext.List',
    xtype: 'menu',

    config: {
        title: 'Menú',

        scrollable: true,
        onItemDisclosure: true,

        itemTpl: [
            '<p><b>{Titol}</b</p>',
            '<small>',
                '<p style="color:#ccc">{Subtitol}</p>',
            '</small>'
        ],

        disableSelection: true

    }
});

Ext.define('Oscars2015.view.Resultats', {
    extend: 'Ext.List',
    xtype: 'resultats',

    config: {
        title: 'Resultats',

        scrollable: true,
        onItemDisclosure: true,

        itemTpl: [
            '<div><b>{Nom}</b></div>',
            '<div style="color:blue">{Guany}€</div>',
        ],

        disableSelection: true,

        store: 'Resultats'
    }
});

    Ext.define('Oscars2015.view.Participants', {
    extend: 'Ext.List',
    xtype: 'participants',

    config: {
        title: 'Participants',

        scrollable: true,

        itemTpl: [
            '<div><b>{Nom}</b></div>',
            '<div style="color:#aaa;font-size:0.8em">Vote Risk: {Risk}</div>',
        ],
        disableSelection: true,

        store: 'Participants'
    }
});

Ext.define('Oscars2015.view.Main', {
    extend: 'Ext.NavigationView',
    xtype: 'main',

    requires: [
        'Ext.TitleBar'
    ],

    config: {
        defaultBackButtonText: 'Enrere',

        navigationBar: {
        }

    }
});

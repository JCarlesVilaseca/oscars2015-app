    Ext.define('Oscars2015.view.ResultatsUsuari', {
    extend: 'Ext.List',
    xtype: 'resultatsusuari',

    config: {
        scrollable: true,

        itemTpl: [
            '<div><b>{Nom}</b></div>',
            '<tpl if="Vot">',
                '<tpl if="Guanyador">',
                    '<tpl if="VotId==Guanyador">',
                        '<p style="color:#aaa;float:right;margin-right:24px;font-size:0.7em">{Guany} ({Vots}/{VotsCategoria})</p>',
                        '<div style="color:green">{Vot}</div>',
                    '<tpl else>',
                        '<p style="color:#aaa;float:right;margin-right:24px;font-size:0.7em">{Guany} ({Vots}/{VotsCategoria})</p>',
                        '<div style="color:red"><strike>{Vot}</strike></div>',
                    '</tpl>',
                '<tpl else>',
                    '<p style="color:#aaa;float:right;margin-right:24px;font-size:0.7em">{Guany} ({Vots}/{VotsCategoria})</p>',
                    '<div style="color:blue">{Vot}</div>',
                '</tpl>',
                '<div style="clear_both"></div>',
            '<tpl else>',
                '<p style="color:#aaa;font-size:0.8em">[Sense vot]</p>',
            '</tpl>'
        ],
        disableSelection: true,

        store: 'Categories'
    }
});

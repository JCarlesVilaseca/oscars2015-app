    Ext.define('Oscars2015.view.Nominats', {
    extend: 'Ext.List',
    xtype: 'nominats',

    config: {
        title: 'Nominats',

        scrollable: true,

        itemTpl: [
            '<div style="float:right;text-align:right">',
		        '<p style="color:#aaa;font-size:0.7em">{Guany} ({Vots}/{VotsCategoria})</p>',
            '</div>',

            '<tpl if="Votat">',
                '<tpl if="Guanyador">',
	               '<div style="color:green"><b>{Titol}</b></div>',
                '<tpl else>',
                   '<div style="color:blue"><b>{Titol}</b></div>',
                '</tpl>',
    	        '<div style="color:#aaa;font-size:0.8em">{Subtitol}</div>',
            '<tpl else>',
	            '<div><b>{Titol}</b></div>',
    	        '<div style="color:#aaa;font-size:0.8em">{Subtitol}</div>',
            '</tpl>',

            '<div style="clear:both"></div>',

            '<tpl if="Guanyador">',
                '<div style="color:green;font-size:0.8em">Guanyador/a</div>',
            '</tpl>'
        ],
        disableSelection: true,

        store: 'Nominats'
    }
});

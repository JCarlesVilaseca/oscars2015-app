Ext.define('Oscars2015.view.Instruccions', {
    extend: 'Ext.Panel',
    xtype: 'instruccions',

    config: {

    	title: 'Instruccions',
    	
    	styleHtmlContent: true,
    	scrollable: 'vertical',

    	html: [
    		'<h2>Porra Oscars 2015 v-1.2</h2>',
    		'<h3>Benvingut a la porra anual dels Oscars. Aquestes són les normes:</h3>',
    		'<ul>',
    			"<li>Cal triar un nominat de cada categoria que creus que guanyarà l'Oscar</li>",
    			"<li>Guanya la porra qui obtingui el major nombre d'euros ficticis</li>",
    			"<li>Cada categoria té un preu diferent diferent, segons la importància de la categoria</li>",
    			"<li>També es té en compte en el recompte d'euros quanta gent ha votat el mateix nominat. Així doncs, es guanya més euros si ets l'únic que tria un nominat i resulta guanyador de l'Oscar. Al mateix estil que ho féiem en la travessa. Com veuràs, es mostra la quantitat de gent que ha votat cada opció i el nombre d'euros a repartir</li>",
    			"<li>Pots votar quan vulguis fins a les 0:00 de la nit de diumenge 22 a dilluns 23 de febrer</li>",
    			"<li>Durant aquest periode pots canviar tantes vegades de vot com vulguis</li>",
    			"<li>Si ja has votat un nominat, pots eliminar el vot tornant a seleccionar-lo</li>",
    			"<li>La mateixa WebApp et mostrarà el resultat de la porra el mateix dilluns. També podràs veure qué han votat els demés</li>",
    		'</ul>',
    		'<h3>Molta sort als participants</h3>',
            '<h4>Novetats v-1.2</h4>',
            '<ul>',
                "<li>Canvi de sistema de punts a euros. Implementat preu per categoria</li>",
                "<li>Control de votació passada l'hora límit</li>",
                "<li>Panell de resultats automàtic a partir de dilluns</li>",
                "<li>Possibilitat de veure vots d'altres a partir de dilluns</li>",
            '</ul>'
    	].join('')
    }
});
